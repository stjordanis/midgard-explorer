THORChain Explorer
=============

Interactive explorer for the THORChain Midgard API. 
- Utilises the byzantine module to ensure Midgard instance is not malicious.
- Fetches API paths from swagger.json and displays available apths in a list
- User can click on each endpoint to display JSON result
- Some endpoints allow the user to click through to other endpoints with selected parameters

WIP! This is a rough outline and should be improved upon. "Ship early & continuously".

## Setup
```bash
yarn
```

## Start
```bash
yarn start
```