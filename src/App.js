import React from 'react';
import { Router } from 'react-router-dom';
import { createBrowserHistory } from 'history';
import byzantine from '@thorchain/byzantine-module';
import ApiContext from 'context/ApiContext';
import MainLayout from 'layouts/MainLayout';

import 'antd/dist/antd.css';
import GlobalStyle from 'Global.style';
import constants from 'config/constants';
import { converToThorchainUrl } from 'utils/utility';

const history = createBrowserHistory();

const App = () => {
  const savedApi = localStorage.getItem(constants.KEY_API);
  const initialApi =
    [constants.API_MIDGARD, constants.API_THORCHAIN].indexOf(savedApi) >= 0
      ? savedApi
      : constants.API_MIDGARD;
  const savedEnv = localStorage.getItem(constants.KEY_ENV);
  const initialEnv =
    [constants.API_ENV_TEST, constants.API_ENV_MAIN].indexOf(savedEnv) >= 0
      ? savedEnv
      : constants.API_ENV_TEST;

  const [data, setData] = React.useState({
    baseUrl: '',
    apiType: initialApi,
    env: initialEnv,
  });

  const setBaseUrl = (url) => updateBaseUrl({ baseUrl: url });
  const setApiType = (type) => {
    updateBaseUrl({ apiType: type });

    localStorage.setItem(constants.KEY_API, type);
  };
  const setApiEnv = (apiEnv) => {
    updateBaseUrl({ env: apiEnv });

    localStorage.setItem(constants.KEY_ENV, apiEnv);
  };

  const updateBaseUrl = (newData = {}) => {
    const updatedData = { ...data, ...newData };
    const isMidgard = updatedData.apiType === constants.API_MIDGARD;
    byzantine(updatedData.env === constants.API_ENV_MAIN).then((baseUrl) => {
      const rightBaseUrl = isMidgard ? baseUrl : converToThorchainUrl(baseUrl);
      setData((prevData) => ({
        ...prevData,
        ...updatedData,
        baseUrl: rightBaseUrl,
      }));
    });
  };

  React.useEffect(() => {
    updateBaseUrl();
  }, []);

  return (
    <Router history={history}>
      <ApiContext.Provider
        value={{
          baseUrl: data.baseUrl,
          setBaseUrl,
          apiType: data.apiType,
          setApiType,
          env: data.env,
          setEnv: setApiEnv,
        }}
      >
        <GlobalStyle />
        <MainLayout />
      </ApiContext.Provider>
    </Router>
  );
};

export default App;
