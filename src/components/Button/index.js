import React from 'react';
import PropTypes from 'prop-types';
import { Button as AntButton } from 'antd';

const defaultStyle = {
  fontFamily: 'Open Sans',
  fontSize: '14px',
  color: '#FFFFFF',
  letterSpacing: 0,
};

const Button = (props) => {
  const {
    style: pStyle,
    bold,
    fill,
    secondary,
    textButton,
    ...restProps
  } = props;
  let style = { ...defaultStyle, ...(pStyle || {}) };
  style.borderRadius = 9;
  if (bold || bold === 'true') {
    style.fontFamily = 'Exo 2';
  }
  if (fill) {
    style.color = '#fff';
    style.backgroundColor = '#4FE1C4';
    style.borderColor = '#33CCFF';
  } else if (secondary) {
    style.color = '#fff';
    style.backgroundColor = '#1C2731';
    style.borderColor = '#4E6376';
  } else if (textButton) {
    style.color = '#23DCC8';
    style.backgroundColor = '#2B3947';
    style.borderColor = '#2B3947';
  } else {
    style.color = '#fff';
    style.backgroundColor = '#000000';
    style.border = '1px solid #4FE1C4';
    style.borderColor = '#4FE1C4';
  }
  return (
    <AntButton style={style} {...restProps}>
      {props.children}
    </AntButton>
  );
};
Button.defaultProp = {
  disabled: false,
  fill: false,
  bold: false,
  loading: false,
};
Button.propTypes = {
  fill: PropTypes.bool,
  bold: PropTypes.bool,
  loading: PropTypes.bool,
};

export default Button;
