import React from 'react';

const defaultStyle = {
  fontFamily: 'Open Sans',
  fontSize: '14px',
  color: '#FFFFFF',
  letterSpacing: 0,
};

const Click = (props) => {
  let style = { ...defaultStyle, ...(props.style || {}) };
  style.fontFamily = 'Exo 2';
  style.fontSize = '24px';
  style.color = '#23DCC8';
  return <span style={style}>{props.children}</span>;
};

export default Click;
