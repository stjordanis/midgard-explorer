import React from 'react';

const defaultStyle = {
  fontFamily: 'Open Sans',
  fontSize: '14px',
  color: '#FFFFFF',
  letterSpacing: 0,
};

const H2 = (props) => {
  let style = { ...defaultStyle, ...(props.style || {}) };
  style.fontFamily = 'Exo 2';
  style.fontSize = '32px';
  return <span style={style}>{props.children}</span>;
};

export default H2;
