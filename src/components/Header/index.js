import React from 'react';
import { Layout, Row, Col } from 'antd';
import byzantine from '@thorchain/byzantine-module';

import { Center, Text } from 'components';
import ApiContext from 'context/ApiContext';

import HeaderWrapper from './index.styles.js';
import constants from 'config/constants';

const Header = () => {
  const {
    baseUrl,
    apiType,
    setApiType,
    env: currentEnv,
    setEnv,
  } = React.useContext(ApiContext);

  const setApiEnv = (newEnv) => {
    return (e) => {
      e.preventDefault();
      setEnv(newEnv);
    };
  };

  const getColor = (env) => {
    return currentEnv === env ? '#FFF' : '#23DCC8';
  };

  const changeApiType = (type) => {
    return (e) => {
      e.persist();
      setApiType(type);
    };
  };

  const getApiColor = (type) => {
    return apiType === type ? '#FFF' : '#23DCC8';
  };

  return (
    <HeaderWrapper>
      <Layout.Header className="header-container">
        <div>
          <span onClick={changeApiType(constants.API_MIDGARD)}>
            <Text size="20px" bold color={getApiColor(constants.API_MIDGARD)}>
              MIDGARD
            </Text>
          </span>
          &nbsp;|&nbsp;
          <span onClick={changeApiType(constants.API_THORCHAIN)}>
            <Text size="20px" bold color={getApiColor(constants.API_THORCHAIN)}>
              THORCHAIN
            </Text>
          </span>
        </div>
        <div>
          <Center>
            <span onClick={setApiEnv(constants.API_ENV_TEST)}>
              <Text size="20px" bold color={getColor(constants.API_ENV_TEST)}>
                TESTNET
              </Text>
            </span>
            &nbsp;
            <span onClick={setApiEnv(constants.API_ENV_MAIN)}>
              <Text size="20px" bold color={getColor(constants.API_ENV_MAIN)}>
                MAINNET
              </Text>
            </span>
          </Center>
        </div>
        <div className="section__base_url">
          <Text>{baseUrl}</Text>
        </div>
      </Layout.Header>
    </HeaderWrapper>
  );
};

export default Header;
