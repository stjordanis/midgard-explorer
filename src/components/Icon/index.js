import React from 'react';
import PropTypes from 'prop-types';

const Icon = (props) => {
  const lookup = {
    plus: 'Asset-plus-grey.svg',
    'coin-rune': 'Coin-RUNE.svg',
    'coin-bep': 'Coin-BEP2.svg',
    'coin-bnb': 'Coin-BNB.svg',
    runelogo: 'Logo-RuneVault.png',
    logo: 'THORChain-white.svg',
    rune: 'rune.png',
    step1: 'step1.svg',
    step2: 'step2.svg',
    openapp: 'ledger-app.svg',
    //"qrcode": "qrCode.svg",
    pincode: 'ledger-pin.svg',
    qrcode: 'qr-code.svg',
  };
  return (
    <img src={'/images/' + lookup[props.icon]} alt={props.img} {...props} />
  );
};

Icon.propTypes = {
  icon: PropTypes.string.isRequired,
  img: PropTypes.string,
};

Icon.defaultProps = {
  img: 'icon-alt',
};

export default Icon;
