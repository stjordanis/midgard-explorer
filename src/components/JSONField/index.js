import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

const JSONFieldContentWrapper = styled.pre`
  font-family: Consolas, Menlo, Monaco, Lucida Console, Liberation Mono,
    DejaVu Sans Mono, Bitstream Vera Sans Mono, Courier New, monospace, serif;
  margin-bottom: 10px;
  margin-top: 25px;
  overflow: auto;
  width: 650px!ie7;
  padding: 5px;
  background-color: #101921;
  padding-bottom: 20px!ie7;
  max-height: 600px;
`;

const JSONField = ({ content }) => {
  return (
    <blockquote>
      <JSONFieldContentWrapper>
        <code>{content ? JSON.stringify(content, null, 4) : null}</code>
      </JSONFieldContentWrapper>
    </blockquote>
  );
};

JSONField.propTypes = {
  content: PropTypes.any,
};

JSONField.defaultProps = {
  content: null,
};

export default JSONField;
