import React from 'react';
import { Spin } from 'antd';
import styled from 'styled-components';

const LoaderWrapper = styled.div`
  position: absolute;
  left: 0;
  right: 0;
  top: 0;
  bottom: 0;
  display: flex;
  justify-content: center;
  align-items: center;
  z-index: 999;
`;

const Loader = () => {
  return (
    <LoaderWrapper>
      <Spin size="large" className="spin" />
    </LoaderWrapper>
  );
};

export default Loader;
