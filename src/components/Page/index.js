import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import Loader from 'components/Loader';
import Text from 'components/Text';

const PageWrapper = styled.div`
  padding: 10px;
  position: relative;
  min-height: 200px;
`;

const Page = ({ loading, title, children }) => {
  return (
    <PageWrapper>
      {title && (
        <div>
          <Text color="#fff" size={18}>
            {title}
          </Text>
          <hr></hr>
        </div>
      )}
      {children}
      {loading && <Loader />}
    </PageWrapper>
  );
};

Page.propTypes = {
  loading: PropTypes.bool,
  title: PropTypes.node,
  children: PropTypes.node,
};

Page.defaultProps = {
  loading: false,
  title: '',
};

export default Page;
