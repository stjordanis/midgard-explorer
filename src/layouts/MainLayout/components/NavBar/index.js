import React, { useContext } from 'react';
import clsx from 'classnames';
import { Layout, Menu } from 'antd';
import { matchPath } from 'react-router';
import NavBarWrapper from './index.styles';
import { Text } from 'components';
import { useRouter } from 'utils/helpers';
import { NavLink } from 'react-router-dom';
import ApiContext from 'context/ApiContext';
import constants from 'config/constants';

const { Sider } = Layout;

const NavBar = (props) => {
  const { apiType } = useContext(ApiContext);

  const { midgardPaths, node } = props;
  const router = useRouter();

  const isMidgard = apiType === constants.API_MIDGARD;

  const renderContent = () => {
    if (!node) {
      return (
        <Text color="#fff" size={18}>
          Byzantine...
        </Text>
      );
    }
    if (isMidgard && !midgardPaths) {
      return (
        <Text color="#fff" size={18}>
          Fetching paths...
        </Text>
      );
    }
    return (
      <Menu className="menu_container" selectable={false}>
        {isMidgard
          ? midgardPaths.map((path) => {
              const isActive = matchPath(router.location.pathname, {
                path,
                exact: false,
              });
              return (
                <Menu.Item
                  key={path}
                  className={clsx('menu_item', isActive ? 'selected' : '')}
                >
                  <NavLink to={path} className="menu_item__nav">
                    {path}
                  </NavLink>
                </Menu.Item>
              );
            })
          : constants.THORCHAIN_ENDPOINTS.map((element) => {
              let isActive = matchPath(router.location.pathname, {
                path: element.endPoint,
                exact: false,
              });
              if (!isActive && element.secondaryEndPoint) {
                isActive = matchPath(router.location.pathname, {
                  path: element.secondaryEndPoint,
                  exact: false,
                });
              }
              return (
                <Menu.Item
                  key={element.id}
                  className={clsx('menu_item', isActive ? 'selected' : '')}
                >
                  <NavLink to={element.endPoint} className="menu_item__nav">
                    {element.label}
                  </NavLink>
                </Menu.Item>
              );
            })}
      </Menu>
    );
  };

  return (
    <NavBarWrapper>
      <Sider className="app-sider">
        <Text color="#fff" size={18}>
          ENDPOINTS
        </Text>
        <hr></hr>
        {renderContent()}
      </Sider>
    </NavBarWrapper>
  );
};

export default NavBar;
