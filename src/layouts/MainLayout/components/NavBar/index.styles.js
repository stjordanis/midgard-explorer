import styled from 'styled-components';

const NavBarWrapper = styled.div`
  background-color: #101921;
  padding: 10px;
  .app-sider {
    height: 100%;
    padding: 10px;
    background-color: #2b3947;
    border: 1px solid #303942;
    border-radius: 5px;
    min-width: 300px !important;
    max-width: 30vw !important;
    width: 100% !important;
    .menu_container {
      background-color: transparent;
      .menu_item {
        height: auto;
        border-top: 1px solid #101921;
        margin: 0;
        padding: 4px;
        transition: all 0.3s;

        &:last-child {
          border-bottom: 1px solid #101921;
        }
        &:hover {
          opacity: 0.6;
        }
        &.selected {
          background-color: #101921;
        }
        .menu_item__nav {
          color: #23dcc8;
          font-size: 24px;
          font-family: Exo 2;
          letter-spacing: 0;
          white-space: break-spaces;
          word-break: break-all;
        }
      }
    }
  }
`;

export default NavBarWrapper;
