import React, { useState, useEffect, useContext } from 'react';
import PropTypes from 'prop-types';
import { renderRoutes } from 'react-router-config';
import { Page, JSONField } from 'components';
import ResponseLayoutWrapper from './index.styles';
import { useRouter, useApiData } from 'utils/helpers';
import { matchPath } from 'react-router';
import ApiContext from 'context/ApiContext';
import constants from 'config/constants';
import midgardRoutes from './midgardRoutes';
import thorchainRoutes from './thorchainRoutes';

const ResponseLayout = (props) => {
  const router = useRouter();
  const { apiType } = useContext(ApiContext);
  const isMidgard = apiType === constants.API_MIDGARD;

  const { pathname, search } = router.location;
  const endPoint = `${pathname}${search}`;

  const apiData = useApiData(endPoint);
  const { currentPath, data, loading } = apiData;

  const isCorrect = matchPath(endPoint, { path: currentPath, exact: true });

  return (
    <ResponseLayoutWrapper>
      <Page title="Response" loading={loading}>
        <div className="response__content">
          {!loading &&
            isCorrect &&
            (isMidgard
              ? renderRoutes(midgardRoutes, apiData)
              : renderRoutes(thorchainRoutes, apiData))}
          {!loading && isCorrect && <JSONField content={data} />}
        </div>
      </Page>
    </ResponseLayoutWrapper>
  );
};

ResponseLayout.propTypes = {};

export default ResponseLayout;
