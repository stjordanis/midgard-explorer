import React from 'react';
import styled from 'styled-components';
import { Row, Col } from 'antd';
import { NavLink } from 'react-router-dom';

const PoolListPageWrapper = styled.div`
  .button__pool {
    font-family: Open Sans;
    font-size: 14px;
    color: rgb(255, 255, 255);
    letter-spacing: 0px;
    height: 40px;
    width: 250px;
    border-radius: 9px;
    background-color: rgb(0, 0, 0);
    border: 1px solid rgb(79, 225, 196);
    cursor: pointer;
  }
`;

const PoolListPage = (props) => {
  const { data: pools } = props;
  if (!pools || typeof pools !== 'object' || !pools.map) return null;
  return (
    <PoolListPageWrapper>
      {pools.map((pool) => (
        <Row key={pool}>
          <Col xs={24} sm={11} md={10} lg={9}>
            <NavLink to={`/v1/pools/detail?asset=${pool}`}>
              <button className="button__pool">{pool}</button>
            </NavLink>
          </Col>
        </Row>
      ))}
    </PoolListPageWrapper>
  );
};

export default PoolListPage;
