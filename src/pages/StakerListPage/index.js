import React from 'react';
import styled from 'styled-components';
import { Row, Col } from 'antd';
import { NavLink } from 'react-router-dom';

const StakerListPageWrapper = styled.div`
  .button__staker {
    font-family: Open Sans;
    font-size: 14px;
    color: rgb(255, 255, 255);
    letter-spacing: 0px;
    height: 40px;
    width: 500px;
    border-radius: 9px;
    background-color: rgb(0, 0, 0);
    border: 1px solid rgb(79, 225, 196);
    cursor: pointer;
  }
`;

const StakerListPage = (props) => {
  const { data: stakers } = props;
  if (!stakers || typeof stakers !== 'object' || !stakers.map) return null;
  return (
    <StakerListPageWrapper>
      {stakers.map((staker) => (
        <Row key={staker}>
          <Col xs={24} sm={11} md={10} lg={9}>
            <NavLink to={`/v1/stakers/${staker}`}>
              <button className="button__staker">{staker}</button>
            </NavLink>
          </Col>
        </Row>
      ))}
    </StakerListPageWrapper>
  );
};

export default StakerListPage;
