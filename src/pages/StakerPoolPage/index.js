import React from 'react';
import styled from 'styled-components';
import { Row, Col } from 'antd';
import { NavLink } from 'react-router-dom';
import { Crumbs } from './components';

const StakerPoolPageWrapper = styled.div`
  .button__pool {
    font-family: Open Sans;
    font-size: 14px;
    color: rgb(255, 255, 255);
    letter-spacing: 0px;
    height: 40px;
    width: 250px;
    border-radius: 9px;
    background-color: rgb(0, 0, 0);
    border: 1px solid rgb(79, 225, 196);
    cursor: pointer;
  }
`;

const StakerPoolPage = (props) => {
  const { currentPath, currentStaker, data: stakerData } = props;
  const showPools = !currentPath.includes('/pools');

  const stakerPools = stakerData.poolsArray;
  return (
    <StakerPoolPageWrapper>
      <Crumbs {...props} />
      {showPools &&
        stakerPools.map((pool) => (
          <Row key={pool}>
            <Col xs={24} sm={11} md={10} lg={9}>
              <NavLink to={`/v1/stakers/${currentStaker}/pools?asset=${pool}`}>
                <button className="button__pool">{pool}</button>
              </NavLink>
            </Col>
          </Row>
        ))}
    </StakerPoolPageWrapper>
  );
};

export default StakerPoolPage;
