import React, { useState } from 'react';
import { useRouter } from 'utils/helpers';
import { matchPath } from 'react-router';
import constants from 'config/constants';
import ThorchainLastBlockPageWrapper from './index.style';
import { Input } from 'antd';
import { Button } from 'components';

const ThorchainLastBlockPage = ({ data }) => {
  const router = useRouter();
  const { pathname } = router.location;

  const heightRoute = '/thorchain/lastblock';

  const isHeightsPage = matchPath(pathname, {
    path: heightRoute,
    exact: true,
  });

  const toChain = () => {
    const tLink = `${heightRoute}/${data.chain}`;
    router.history.push(tLink);
  };

  const onBack = () => {
    router.history.push(heightRoute);
  };

  return (
    <ThorchainLastBlockPageWrapper>
      <div>{!isHeightsPage && <Button onClick={onBack}>Heights</Button>}</div>
      <div>
        {isHeightsPage && <Button onClick={toChain}>Chain Heights</Button>}
      </div>
    </ThorchainLastBlockPageWrapper>
  );
};

export default ThorchainLastBlockPage;
