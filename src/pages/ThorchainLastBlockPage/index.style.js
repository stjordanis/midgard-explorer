import styled from 'styled-components';

const ThorchainLastBlockPageWrapper = styled.div`
  margin-top: 20px;
  display: flex;
  justify-content: space-between;
  align-items: center;
`;

export default ThorchainLastBlockPageWrapper;
