import React from 'react';
import ThorchainNodeAccountsPageWrapper from './index.style';
import { NavLink } from 'react-router-dom';
import { Select } from 'antd';
import { useState } from 'react';

const { Option } = Select;

const ThorchainNodeAccountsPage = (props) => {
  const { data } = props;
  const [state, setState] = useState({
    filterValue: 'all',
  });
  const possibleStatuses = { all: true };

  data.forEach((item) => {
    possibleStatuses[item.status] = true;
  });

  const filterChange = (value) => {
    setState((prevState) => ({ ...prevState, filterValue: value }));
  };

  return (
    <ThorchainNodeAccountsPageWrapper>
      <Select
        className="filter-select"
        value={state.filterValue}
        onChange={filterChange}
      >
        {Object.keys(possibleStatuses).map((status) => (
          <Option value={status} key={status}>
            {status}
          </Option>
        ))}
      </Select>
      {data
        .filter(
          (item) =>
            state.filterValue === 'all' || state.filterValue === item.status
        )
        .map((item) => (
          <div key={item.node_address}>
            <NavLink
              className="link__pool"
              to={`/thorchain/nodeaccount/${item.node_address}`}
            >
              {item.node_address}
            </NavLink>
          </div>
        ))}
    </ThorchainNodeAccountsPageWrapper>
  );
};

export default ThorchainNodeAccountsPage;
