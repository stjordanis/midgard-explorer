import React from 'react';
import ThorchainPoolPageWrapper from './index.style';
import { NavLink } from 'react-router-dom';
import { useRouter } from 'utils/helpers';

const ThorchainPoolPage = () => {
  const {
    location: { pathname },
  } = useRouter();

  return (
    <ThorchainPoolPageWrapper>
      <NavLink className="link__button" to="/thorchain/pools">
        Pools
      </NavLink>
      <NavLink className="link__button" to={`${pathname}/stakers`}>
        Stakers
      </NavLink>
    </ThorchainPoolPageWrapper>
  );
};

export default ThorchainPoolPage;
