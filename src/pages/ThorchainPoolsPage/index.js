import React from 'react';
import ThorchainPoolsPageWrapper from './index.style';
import { NavLink } from 'react-router-dom';

const ThorchainPoolsPage = (props) => {
  const { data } = props;

  return (
    <ThorchainPoolsPageWrapper>
      {data.map((pool) => (
        <div key={pool.asset}>
          <NavLink className="link__pool" to={`/thorchain/pool/${pool.asset}`}>
            {pool.asset}
          </NavLink>
        </div>
      ))}
    </ThorchainPoolsPageWrapper>
  );
};

export default ThorchainPoolsPage;
