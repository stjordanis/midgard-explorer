import React, { useState } from 'react';
import { useRouter } from 'utils/helpers';
import { matchPath } from 'react-router';
import constants from 'config/constants';
import ThorchainTransactionPageWrapper from './index.style';
import { Input } from 'antd';
import { Button } from 'components';
import { NavLink } from 'react-router-dom';

const ThorchainTransactionPage = () => {
  const router = useRouter();
  const { pathname } = router.location;
  const transactionRoute = constants.THORCHAIN_SHOW_PATHS[0];

  const showInput = matchPath(pathname, {
    path: transactionRoute,
    exact: true,
  });

  const [tId, setTId] = useState('');

  const handleChangeTId = (event) => {
    event.persist();
    setTId(event.target.value);
  };

  const toTransactionDetails = () => {
    const tLink = `${transactionRoute}/${tId}`;
    router.history.push(tLink);
  };

  const onBack = () => {
    router.history.push(transactionRoute);
  };

  const renderInputContent = () => {
    return (
      <div className="content__input">
        <br />
        <Input
          placeholder="Transaction Id"
          value={tId}
          onChange={handleChangeTId}
        />
        <br />
        <br />
        <Button disabled={tId === ''} onClick={toTransactionDetails} secondary>
          Show Transaction Info
        </Button>
      </div>
    );
  };

  const renderBackContent = () => {
    return (
      <div>
        <br />
        <Button onClick={onBack} secondary>
          Edit Transaction ID?
        </Button>
      </div>
    );
  };

  return (
    <ThorchainTransactionPageWrapper>
      {showInput ? renderInputContent() : renderBackContent()}
    </ThorchainTransactionPageWrapper>
  );
};

export default ThorchainTransactionPage;
