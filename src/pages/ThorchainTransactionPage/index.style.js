import styled from 'styled-components';

const ThorchainTransactionPageWrapper = styled.div`
  .content__input {
    text-align: right;
  }
`;

export default ThorchainTransactionPageWrapper;
