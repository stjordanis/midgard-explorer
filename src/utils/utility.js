// replacement for .toLocaleString(). That func does rounding that I don't like.
const AmounttoString = (amount) => {
  // Converting to string with rounding to 8 digits
  var parts = amount
    .toPrecision(8)
    .replace(/\.?0+$/, '')
    .split('.');
  parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ',');
  return parts.join('.');
};

function formatDate(lastUpdatedDate) {
  return lastUpdatedDate
    .toLocaleDateString('en-GB', {
      day: 'numeric',
      month: 'long',
      year: 'numeric',
    })
    .replace(/ /g, ' ');
}

function converToThorchainUrl(baseUrl) {
  const splits = String(baseUrl).split(':');
  let endPointUrl;

  if (splits.length === 2) {
    endPointUrl = baseUrl;
  } else {
    endPointUrl = splits.slice(0, splits.length - 1).join(':');
  }

  return `${endPointUrl}:1317`;
}

export { AmounttoString, formatDate, converToThorchainUrl };
